/*
upset capital experimentalizes careful comradeship
le capital bouleversé expérimente la camaraderie prudente
*/


var Helpers = (function () {
	return {

		// TODO: Helpers Ajax.get / .post / …
		Ajax : {

			get : function (url) {
				return new Promise (function (resolve, reject) {
					let req = new XMLHttpRequest();
					req.open("GET", url);
					req.onload = function() {
						if (req.status === 200) {
							resolve (req.response);
						} else {
							reject (new Error (req.statusText));
						}
					};
					req.onerror = function() {
						reject (new Error("Network error"));
					};
					req.send();
				});
			},

			post : {},
		},

	}
})();




const passphrase = (function (Helpers) {
	"use strict";


	const State = {
		Ajax : Helpers.Ajax,
		words : {
			'nouns': [],
			'adjectives': [],
			'verbs': [],
		},
		lang: 'en',
		urls: {
			'en': {
				'nouns': '/data/nouns.json',
				'adjectives': '/data/adjectives.json',
				'verbs': '/data/verbs.json',
			},
			'fr': {
				'nouns': '/data/french-nouns-v2.json',
				'adjectives': '/data/french-adjectives-v2.json',
				'verbs': '/data/french-verbs-v2.json',
			}
		},
		baffleOptions : {
			speed : 50,
			characters: 'ABCDEFGHIJKLMNOPQRSTUVWXYZΑBΓΔEZHΘIKΛNΞΟΠPΣTΥΦΧΨΩ',
			// characters : '░▒▓💣А​‌Б​‌В​‌Г​‌Ґ​‌Д​‌Ђ​‌Е​‌Ё​‌Є​‌Ж​‌З​‌Ѕ​‌И​‌І​‌Ї​‌Й​‌Ј​‌К​‌Л​‌Љ​‌М​‌Н​‌Њ​‌О​‌П​‌Р​‌С​‌Т​‌Ћ​‌У​‌Ў​‌Ф​‌Х​‌Ц​‌Ч​‌Џ​‌Ш​‌Щ​‌Ъ​‌Ы​‌Ь​‌Э​‌Ю​‌ЯΑ​‌Β​‌Γ​‌Δ​‌Ε​‌Ζ​‌Η​‌Θ​‌Ι​‌Κ​‌Λ​‌Μ​‌Ν​‌Ξ​‌Ο​‌Π​‌Ρ​‌Σ​‌Τ​‌Υ​‌Φ​‌Χ​‌Ψ​‌Ω​‌α​‌β​‌γ​‌δ​‌ε​‌ζ​‌η​‌θ​‌ι​‌κ​‌λ​‌μ​‌ν​‌ξ​‌ο​‌π​‌ρ​‌σ​‌τ​‌υ​‌φ​‌χ​‌ψ​‌ω​‌@​‌&​‌®​‌©​‌$​‌€​‌£​‌¥​‌​‌*',
			// characters : '💣ΑBΓΔEϜZHΘIKΛMNΞΟΠϺϘPΣTΥΦΧΨΩ~!@#$%^&*()-+=[]{}|;:,./<>?᐀—⑈⑉⑊🤖',
			// characters : '░▒▓💣ΑBΓΔEϜZHΘIKΛMNΞΟΠϺϘPΣTΥΦΧΨΩ~!@#$%^&*()-+=[]{}|;:,./<>?᐀—⑈⑉⑊🤖',
			// characters : '░▒▓▙▚▛▜▝▞▟',
			exclude : [' '],
			revealSpeed : 1000,
			revealDelay : 50,
		}
	};


	const getRandomVerb = function () {
		let random = Math.floor (Math.random () * State.words.verbs.length);
		return State.words.verbs[random];
	};

	const getRandomAdjective = function () {
		let random = Math.floor (Math.random () * State.words.adjectives.length);
		return State.words.adjectives[random];
	};

	const getRandomNoun = function () {
		let random = Math.floor (Math.random () * State.words.nouns.length);
		return State.words.nouns[random];
	};


	const cacheWords = function () {
		localStorage.setItem ('words', JSON.stringify (State.words));
	};



	const loadData = function (lang) {

		lang = lang || State.lang;

		// Nouns
		let getNouns = State.Ajax.get (State.urls[lang].nouns);

		getNouns.then (function (result) {
			State.words.nouns = JSON.parse (result);
			cacheWords ();
		});
		getNouns.catch (function (error) {
			throw new JSONError (error);
		});

		// Adjectives
		let getAdjectives = State.Ajax.get (State.urls[lang].adjectives);

		getAdjectives.then (function (result) {
			State.words.adjectives = JSON.parse (result);
			cacheWords ();
		})
		getAdjectives.catch (function (error) {
			throw new JSONError (error);
		});

		// Verbs
		let getVerbs = State.Ajax.get (State.urls[lang].verbs);

		getVerbs.then (function (result) {
			State.words.verbs = JSON.parse (result);
			cacheWords ();
		})
		getVerbs.catch (function (error) {
			throw new JSONError (error);
		});

		return [getNouns, getAdjectives, getVerbs];
	};



	const randomImage = function (randomPassphrase) {
		let words = randomPassphrase.split (' ');
		let imageUrl = `https://source.unsplash.com/featured/?${words[0]},${words[1]},${words[2]},${words[3]}`;
		let imageWrapper = document.querySelector ('[data-wrapper]');
		imageWrapper.style.backgroundImage = `url(${imageUrl})`;
		imageWrapper.classList.add ('loaded');
};



const displayPassphrase = function (randomPassphrase) {
	let output = document.querySelector ('[data-input-passphrase]');
	output.innerText = randomPassphrase;

	let animation = State.baffle || baffle (output, State.baffleOptions);
	animation.stop ();
	animation.start().reveal (State.baffleOptions.revealSpeed, State.baffleOptions.revealDelay);
};



const generatePassphrase = function () {
	let verb = getRandomVerb ();
	let nouns = [getRandomNoun (), getRandomNoun ()];
	let adjectives = [getRandomAdjective (), getRandomAdjective ()];
	let randomPassphrase = '';

	if (State.lang == 'fr') {
		randomPassphrase = `${nouns[0]} ${adjectives[0]} ${verb} ${nouns[1]} ${adjectives[1]}`.toLowerCase();
	}
	else {
		randomPassphrase = `${adjectives[0]} ${nouns[0]} ${verb} ${adjectives[1]} ${nouns[1]}`.toLowerCase();
	}

	console.info (State);
	displayPassphrase (randomPassphrase);
};


const start = function () {
	// if data, display
	if (State.words.nouns.length > 0 && State.words.adjectives.length > 0 && State.words.verbs.length > 0) {
		return generatePassphrase ();
	}

	// load data
	let dataLoaded = loadData ();
	Promise.all (dataLoaded).then (words => {
		return generatePassphrase ();
	});
};


const resetCache = function () {
	return localStorage.clear ();
};


const setLanguage = function () {
	// cached setting
	let currentLang = localStorage.getItem ('lang');
	if (currentLang) State.lang = currentLang;

	// url
	let url = new URLSearchParams (window.location.search);
	let lastLang = State.lang;

	State.lang = url.has('lang') ? url.get('lang') : State.lang;
	if (lastLang != State.lang) resetCache ();

	localStorage.setItem ('lang', State.lang);
};



const init = function () {

	// set language (with cache invalidation)
	// setLanguage ();

	// load cached data
	let localWords = localStorage.getItem ('words');
	if (localWords) State.words = JSON.parse (localWords);

	// copy to clipboard
	var clipboard = new Clipboard ('[data-button-copy]');

	start ();
	document.querySelector ('[data-button-generate]').addEventListener ('click', start);
};


return {
	init: init,
	generate: generatePassphrase,
	display: displayPassphrase,
}

})(Helpers);


passphrase.init ();
